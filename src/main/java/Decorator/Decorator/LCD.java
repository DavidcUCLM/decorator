package Decorator.Decorator;

interface LCD {
    void write(String[] s);
    void read(String[] s);
}